import tensorflow as tf
import pandas as pd
import numpy as np

# Function to get Batch


def next_batch(batch_size, data, y_labels, current_batch):
    start = current_batch * batch_size
    end = start + batch_size
    return {X: data[start:end], y: y_labels[start:end]}

#
# Data Import
#


# Data Import Path
path_images = './dataset/images/'
path_labels = './dataset/labels.csv'


# Functions
def search_file(keyword, files):
    index = 0
    for index in range(len(files)):
        if keyword in files[index]:
            return index

# Import Images


from os import listdir
from skimage.io import imread

file_names = pd.read_csv(path_labels)['PIC'].values
image_files = listdir(path_images)

raw_image_data = []
faulty_files = []
i = 0

for file in file_names:
    i += 1
    try:
        index = search_file(file.replace('-', '.'), image_files)
        raw_image_data.append(
            imread(path_images + image_files[index], as_gray=True))
    except:
        print("Imgage Load Error: ", file)
        faulty_files.append(i)

print('\nImport Errors: ', faulty_files)
print('Image Imports: ', len(raw_image_data))

formatted = np.array(raw_image_data).reshape(
    (-1, 256, 256, 1)).astype(np.float32)

# Label Import

file_names = pd.read_csv(path_labels)['PIC'].values

raw_label_data = pd.read_csv(path_labels).drop(['#', 'PIC'], 1)
raw_label_data.drop(faulty_files, inplace=True)

print(raw_label_data.head())

labels = np.array(raw_label_data.values)

print('Values Shape: ', labels.shape)

# Other Variables

n_attributes = len(labels[0])
picture_size = raw_image_data[0].shape[0]

print('n_attributes ', n_attributes)
print('picture_size ', picture_size)

#
# Neural Network Construction
#

# Variablen
X = tf.placeholder(tf.float32, shape=(None, 256, 256, 1), name='X')
y = tf.placeholder(tf.float32, shape=(None, 6), name='y')

# DNN Layers
with tf.name_scope('Layer1'):

    conv1 = tf.layers.conv2d(
        X,
        filters=16,
        kernel_size=6,
        strides=[2, 2],
        padding='SAME',
        activation=tf.nn.relu
    )

    pool1 = tf.layers.max_pooling2d(
        conv1,
        2,
        2,
        padding='VALID'
    )

with tf.name_scope("Layer2"):
    conv2 = tf.layers.conv2d(
        pool1,
        filters=32,
        kernel_size=3,
        strides=[2, 2],
        padding='SAME',
        activation=tf.nn.relu
    )

    pool2 = tf.layers.max_pooling2d(
        conv2,
        2,
        2,
        padding='VALID'
    )

with tf.name_scope("FlattenDropoutLayer"):
    flatten = tf.layers.flatten(pool2)
    dropout = tf.layers.dropout(flatten, 0.6)


with tf.name_scope("DenseLayer"):
    dense1 = tf.layers.dense(dropout, 512, activation=tf.nn.relu)
    dense2 = tf.layers.dense(dense1, 256, activation=tf.nn.relu)

with tf.name_scope("OutputLayer"):
    logits = tf.layers.dense(dense2, n_attributes,
                             name='logits', reuse=tf.AUTO_REUSE)


# Loss and Train

with tf.name_scope('loss'):
    loss = tf.losses.mean_squared_error(y, logits)

with tf.name_scope('train'):
    optimizer = tf.train.AdamOptimizer(0.001)
    training_op = optimizer.minimize(loss)

#
# Training
#

n_epochs = 20
batch_size = 40
restore = True

init = tf.global_variables_initializer()
saver = tf.train.Saver()


with tf.Session() as sess:
    if restore:
        saver.restore(sess, './model/v1-prototype.ckpt')
    else:
        init.run()

    for epoch in range(n_epochs):

        for batch in range(len(labels) // batch_size):
            batch_dict = next_batch(batch_size, formatted, labels, batch)

            sess.run(training_op, batch_dict)
        loss_eval = loss.eval(
            feed_dict={X: formatted[40:80], y: labels[40:80]})
        print('Epoch', epoch, 'Loss ', loss_eval)

        # Saving Model
        saver.save(sess, './model/v1-prototype.ckpt')
    test = logits.eval(feed_dict={X: formatted[40:45], y: labels[40:45]})
